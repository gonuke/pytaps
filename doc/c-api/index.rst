=======
 C API
=======

PyTAPS can be extended from C. Note that this documentation is currently
incomplete and the API itself is subject to changes (especially ABI changes).

.. toctree::
   :maxdepth: 2

   ibase
   imesh
   igeom
   irel
