========
 PyTAPS
========

PyTAPS is a Python package designed to interface with the `ITAPS
<http://www.tstt-scidac.org/>`_, (Interoperable Technologies for Advanced
Petascale Simulations) interfaces, a set of standardized interfaces for use in
simulation code written in C, C++, or Fortran. PyTAPS focuses on support for the
`Fathom <http://trac.mcs.anl.gov/projects/fathom>`_ project
(`MOAB <http://trac.mcs.anl.gov/projects/ITAPS/wiki/MOAB/>`_,
`CGM <http://trac.mcs.anl.gov/projects/ITAPS/wiki/CGM/>`_, and
`Lasso <http://trac.mcs.anl.gov/projects/ITAPS/wiki/Lasso/>`_).

.. toctree::
   :maxdepth: 2

   install
   tutorial/index
   ibase
   imesh
   igeom
   irel
   helpers
   c-api/index
