__all__ = ['adj', 'basic', 'coords', 'creation', 'diff', 'entset', 'iter',
           'tags']
__requires__ = ['itaps.iGeom']
